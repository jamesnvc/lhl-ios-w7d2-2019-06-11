//
//  ViewController.swift
//  AnimationDemo
//
//  Created by James Cash on 11-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var container: UIView!
    var label: UILabel!
    var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        let v1 = UIView(frame: CGRect(x: 10, y: 10, width: 100, height: 100))
        v1.backgroundColor = UIColor.purple
        view.addSubview(v1)

        UIView.animate(withDuration: 2, delay: 0.5,
                       options: [.repeat, .autoreverse],
                       animations: {
                        v1.center = self.view.center
                        v1.backgroundColor = UIColor.red
        }, completion: { (done) in
            print("this never gets called (because .repeat)")
        })

        let v2 = UIView(frame: CGRect(x: 10, y: 300, width: 100, height: 100))
        v2.backgroundColor = UIColor.magenta
        view.addSubview(v2)
        UIView.animateKeyframes(
            withDuration: 3,
            delay: 0,
            options: [.repeat, .autoreverse],
            animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                    v2.transform = CGAffineTransform(rotationAngle: CGFloat.pi*0.5)
                    v2.backgroundColor = UIColor.green
                })
                UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.2, animations: {
                    v2.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                    v2.backgroundColor = UIColor.red
                })
                UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                    v2.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2)
                    v2.backgroundColor = UIColor.orange
                })
        },
            completion: nil)

        container = UIView(frame: CGRect(x: 50, y: 250, width: 300, height: 500))
        container.backgroundColor = UIColor.lightGray
        view.addSubview(container)

        button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Swap", for: .normal)
        button.addTarget(self, action: #selector(swapSubviews(_:)), for: .touchUpInside)
        container.addSubview(button)
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: container.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: container.centerYAnchor),
        ])

        label = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 44))
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Beep boop"
    }

    @objc func swapSubviews(_ sender: UIButton) {
        UIView.transition(with: container,
                          duration: 3,
                          options: [.transitionFlipFromTop],
                          animations: {
                            self.button.removeFromSuperview()
                            self.container.addSubview(self.label)
                            NSLayoutConstraint.activate([
                                self.label.centerXAnchor.constraint(equalTo: self.container.centerXAnchor),
                                self.label.centerYAnchor.constraint(equalTo: self.container.centerYAnchor)
                            ])
                            self.container.layoutIfNeeded()
        }, completion: nil)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // view.layer.speed *= 2.0
    }

}

